/*
 ** Expression Parser v1.0 **
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Author: Lisa
 * Email: syn.shainer@gmail.com
 * Website: http://giudoku.sourceforge.net
 * 
 * Compiling: gcc ExpressionParser.c -lm
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#define MAX_CHARS 255
#define PR_SIZE     7
#define MAX_VAL    50

/*
 * Constants for the errors
 */
#define INVALID_CHAR        1
#define BAD_ALLOC           2
#define MISSING_OPERATOR    3
#define MISSING_VALUE       4
#define STRANGE_BRACKETS    5

/*
 * An enumeration describing the operators
 * T_LPR and T_RPR represent the brackets
 * T_ERROR is a special value for error conditions
 */
typedef enum
{
	 T_ADD, T_SUB, T_MUL, T_DIV, T_POW, T_MINUS, T_LPR, T_EOF, T_RPR, T_VAL, T_ERROR
} Operators;

/*
 * Base structs for the two stacks
 */
struct operator
{
	Operators sOp;
	struct operator *next;
};
struct value
{
	double n;
	struct value *next;
};
typedef struct operator SOP_t;
typedef struct value value_t;

int errorCode; /* similar to the Unix errno */

double parseExpression(char *);
Operators popOperator(SOP_t **);
double popValue(value_t **);
void shiftOnOperators(SOP_t **, Operators);
void shiftOnValues(value_t **, double);
double reduce(SOP_t **, value_t **);

void getExpression(char *);
Operators getNextOperator(char **, char *, SOP_t *, value_t *);
int lookFor(SOP_t *, Operators);
int isAcceptedChar(char);

void deAllocation(SOP_t *, value_t *);
void exitOnError(int, SOP_t *, value_t *);

int main()
{
	char equation[MAX_CHARS] = {0};
	
	printf("Enter expression: ");
	getExpression(equation);
	printf("The result is %.6f\n", parseExpression(equation));
	
	return 0;
}

/*
 * The central function
 */
double parseExpression(char *equation)
{
	SOP_t *sHead = NULL;
	value_t *vHead = NULL;
	Operators curOp;
	double res;
	
	/* 
	 * Precedence matrix to decide the next action
	 * R is reduce and S is shift
	 * */
	char precedence[PR_SIZE][PR_SIZE] = {{'R', 'R', 'S', 'S', 'S', 'S', 'S'}, {'R', 'R', 'S', 'S', 'S', 'S', 'S'}, {'R', 'R', 'R', 'R', 'S', 'S', 'S'}, {'R', 'R', 'R', 'R', 'S', 'S', 'S'}, {'R', 'R', 'R', 'R', 'S', 'R', 'S'}, {'R', 'R', 'R', 'R', 'R', 'R', 'S'}, {'S', 'S', 'S', 'S', 'S', 'S', 'S'}};
	char num[MAX_VAL] = {'\0'}; /* temporary storage for numbers */
	
	do
	{
		curOp = getNextOperator(&equation, num, sHead, vHead);
		
		if (curOp == T_VAL) /* values are always shifted on the stack */
		{
			shiftOnValues(&vHead, atof(num));
		}
		else if (curOp < T_EOF) /* we have an operator */
		{
			if (!sHead || precedence[sHead->sOp][curOp] == 'S')
			{	
				shiftOnOperators(&sHead, curOp);
			}
			else
			{
				res = reduce(&sHead, &vHead);
				shiftOnValues(&vHead, res); /* shifts the result of the operation */
				shiftOnOperators(&sHead, curOp);
			}
		}
		else if (curOp == T_RPR)
		{
			/* First checks if the bracket has been opened */
			if (lookFor(sHead, T_LPR) == -1)
			{
				exitOnError(STRANGE_BRACKETS, sHead, vHead);
			}
			
			/*
			 * Reduces everything until the bracket
			 */
			while (sHead->sOp != T_LPR)
			{
				res = reduce(&sHead, &vHead);
				shiftOnValues(&vHead, res);
			}
			curOp = popOperator(&sHead); /* discards the bracket in question */
		}
		
		if (errorCode != 0) /* an error occured! */
		{
			exitOnError(errorCode, sHead, vHead);
		}
	} while (curOp != T_EOF);
	
	/*
	 * Final reducing to free the stacks
	 */
	while (sHead)
	{
		if (sHead->sOp == T_LPR)
		{
			exitOnError(STRANGE_BRACKETS, sHead, vHead);
		}
		res = reduce(&sHead, &vHead);
		shiftOnValues(&vHead, res);
		
		if (errorCode != 0)
		{
			exitOnError(MISSING_VALUE, sHead, vHead);
		}
	}
	
	/*
	 * Something remained on the value stack
	 * and shouldn't be there
	 */
	if (vHead->next)
	{
		exitOnError(MISSING_OPERATOR, sHead, vHead);
	}
	
	/*
	 * At this point, if the input was correct the operator stack is empty
	 * while the value stack contains only the result on top
	 */
	return (vHead->n);
}

/*
 * Pushes a new operator on the stack
 */
void shiftOnOperators(SOP_t **head, Operators o)
{
	SOP_t *node = (SOP_t *)malloc(sizeof(struct operator));
	
	if (!node)
	{
		errorCode = BAD_ALLOC;
		return;
	}	
	node->sOp = o;
	node->next = *head;
	*head = node;
}

/*
 * Pushes a new value on the stack
 */
void shiftOnValues(value_t **head, double v)
{
	value_t *node = (value_t *)malloc(sizeof(value_t));
	
	if (!node)
	{
		errorCode = BAD_ALLOC;
		return;
	}
	
	node->n = v;
	node->next = *head;
	*head = node;
}

/*
 * Pops the operator on top (if present)
 */
Operators popOperator(SOP_t **sHead)
{
	SOP_t *temp = (*sHead);
	Operators op;
	
	if (!temp) /* the stack is empty */
	{
		errorCode = MISSING_OPERATOR;
		return T_ERROR;
	}
	op = temp->sOp;
	*sHead = (*sHead)->next;
	free(temp);
	
	return op;
}

/*
 * Pops the value on top (if present)
 */
double popValue(value_t **vHead)
{
	value_t *temp = *vHead;
	double p;
	
	if (!temp) /* The stack is empty */
	{
		errorCode = MISSING_VALUE;
		return 0.0f;
	}
	p = temp->n;
	*vHead = (*vHead)->next;
	free(temp);
	
	return p;
}

/*
 * It takes the first operator on the stack, then applies it to the operand(s)
 * taken from the other stack.
 * The result is given back to the caller
 */
double reduce(SOP_t **sHead, value_t **vHead)
{
	Operators toApply = popOperator(sHead);
	double f1, f2, res;
	
	f1 = popValue(vHead);
	if (toApply != T_MINUS) /* the only unary operator */
	{
		f2 = popValue(vHead);
	}
	
	switch (toApply)
	{
		case T_ADD:
			res = f2 + f1;
			break;
		
		case T_SUB:
			res = f2 - f1;
			break;
		
		case T_MINUS:
			res = -f1;
			break;
		
		case T_MUL:
			res = f2 * f1;
			break;
		
		case T_DIV:
			res = f2 / f1;
			break;
		
		case T_POW:
			res = pow(f2, f1);
			break;
		
		default:
			res = 0.0f; /* random value */
			break;
	}
	
	return res;
}

/*
 * Get the next operator or numeric value from the input
 * The pointers for the stacks are useful just in one case (to decide if a - is a minus or a subtraction operator.
 * If op is T_VAL, num will contain the numeric value involved
 */
Operators getNextOperator(char **eq, char *num, SOP_t *sHead, value_t *vHead)
{
	int index = 0;
	Operators op;
	
	memset((void *)num, 0, MAX_VAL); /* zeroes the memory */
	
	/*
	 * Goes on if it meets some spaces
	 * (more comfortable than doing a separate procedure for removing them)
	 */
	while (*eq && **eq == ' ')
	{
		(*eq)++;
	}
	
	if (**eq != 0)
	{
		switch(**eq)
		{
			case '*':
			op = T_MUL;
			break;
			
			case '+':
			op = T_ADD;
			break;
			
			case '-':
			/*
			 * If it's the first thing present (like -4 + 5) or a bracket has been opened,
			 * like 5 * (-4), then it's a minus operator
			 */
			if ((!sHead && !vHead) || (sHead && sHead->sOp == T_LPR))
			{
				op = T_MINUS;
			}
			else
			{
				op = T_SUB;
			}
			break;
			
			case '/':
			op = T_DIV;
			break;
			
			case '^':
			op = T_POW;
			break;
			
			case '(':
			op = T_LPR;
			break;
			
			case ')':
			op = T_RPR;
			break;
			
			default:
				if (isdigit(**eq))
				{
					op = T_VAL;
					while ((isdigit(**eq) || **eq == '.') && *eq && index < MAX_VAL)
					{
						num[index++] = **eq;
						(*eq)++;
					}
					
					if (isAcceptedChar(**eq) == -1) /* the character right after the digit could be invalid, like 3a */
					{
						errorCode = INVALID_CHAR;
						return T_ERROR;
					}
				}
				else /* an unknown character has been encountered */
				{
					errorCode = INVALID_CHAR;
					return T_ERROR;
				}
				return op;
		}
		(*eq)++;
	}
	else
	{
		op = T_EOF;
	}
	
	return op;
}

/*
 * Very quick function to look for an element in the operator stack
 * Return 1 on success, -1 otherwise
 */
int lookFor(SOP_t *head, Operators o)
{
	SOP_t *temp = head;
	
	while (temp)
	{
		if (temp->sOp == o)
		{
			return 1;
		}
		temp = temp->next;
	}
	
	return -1;
}

/*
 * Input function
 * Every input after the 255th character will be truncated
 */
void getExpression(char *eq)
{
	int i;
	char chr;
	
	for (i = 0; (i < MAX_CHARS) && ((chr=getchar()) != '\n'); i++)
	{
		if (chr != '\b')
		{
			eq[i] = chr;
		}
	}
}

/*
 * Checks if a given char is an acceptable input
 */
int isAcceptedChar(char ch)
{
	char good[10] = {'+', '-' , '*', '/', '^', '(', ')', ' ', '.', '\0'};
	int i;
	
	for (i = 0; i < 10; i++)
	{
		if (good[i] == ch)
		{
			return 1;
		}
	}
	
	return -1;
}

/*
 * Used only in case of errors, before exiting the program
 * Deallocates the two stacks to prevent memory leaks
 */
void deAllocation(SOP_t *sHead, value_t *vHead)
{
	SOP_t *temp1;
	value_t *temp2;
	
	if (sHead)
	{
		for (temp1 = sHead->next; temp1; temp1 = temp1->next)
		{
			sHead->next = temp1->next;
			free(temp1);
		}
	}
	if (vHead)
	{
		for (temp2 = vHead->next; temp2; temp2 = temp2->next)
		{
			vHead->next = temp2->next;
			free(temp2);
		}
	}
	
	free(sHead);
	free(vHead);
}

/*
 * Free the resources and prints an error message
 */
void exitOnError(int code, SOP_t *s, value_t *v)
{
	char *errors[6] = {"Invalid character in input", "Error in allocation", "Missing operator", "Missing value", "Missing a bracket somewhere", NULL};

	deAllocation(s, v);	
	fprintf(stderr, "[ERR] %s\n", errors[code-1]);
	exit(-1);
}
